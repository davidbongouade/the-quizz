import 'package:flutter/material.dart';

import 'package:rflutter_alert/rflutter_alert.dart';

import '../controllers/quizz_controller.dart';
import '../ressources/quizz_builder.dart';

QuizzController quizzController = QuizzController();

class QuizzScreen extends StatefulWidget {
  @override
  _QuizzScreenState createState() => _QuizzScreenState();
}

class _QuizzScreenState extends State<QuizzScreen> {
  List<Widget> _score = [];
  List<Widget> _randomButton = [];
  int _counter = 0;
  int _point = 0;
  int _numberQuestions = 10;
  var alertStyle = AlertStyle(
    isOverlayTapDismiss: false,
    titleStyle: TextStyle(
      color: Colors.red,
    ),
  );

  @override
  void initState() {
    super.initState();
    quizzController.shuffleQuestions();
    _randomButton = [
      QuizzBuilder.buildButton(
          name: "Vrai",
          onTap: () {
            setState(() {
              if (quizzController.getAnswer(_counter) == true) {
                if (_score.length < _numberQuestions) {
                  _score.add(QuizzBuilder.check());
                  _point++;
                }
              } else {
                if (_score.length < _numberQuestions) {
                  _score.add(QuizzBuilder.close());
                }
              }
              increment();
            });
          },
          color: Colors.green),
      QuizzBuilder.buildButton(
          name: "Faux",
          onTap: () {
            setState(() {
              if (quizzController.getAnswer(_counter) == false) {
                if (_score.length < _numberQuestions) {
                  _score.add(QuizzBuilder.check());
                  _point++;
                }
              } else {
                if (_score.length < _numberQuestions) {
                  _score.add(QuizzBuilder.close());
                }
              }
              increment();
            });
          },
          color: Colors.red),
    ];
  }

  void resetQuizz() {
    Alert(
        context: context,
        style: alertStyle,
        type: AlertType.info,
        title: "Fin du quizz",
        desc:
            "Votre scrore est de\n$_point / $_numberQuestions \n\nLe quizz est terminé, voulez-vous le relancer ?",
        buttons: <DialogButton>[
          QuizzBuilder.buttonDial(
            name: "Non",
            onTap: () {
              Navigator.pop(context);
            },
          ),
          QuizzBuilder.buttonDial(
            name: "Oui",
            onTap: () {
              Navigator.pop(context);
              setState(() {
                quizzController.shuffleQuestions();
                _counter = 0;
                _point = 0;
                _score.clear();
              });
            },
          ),
        ]).show();
  }

  void increment() {
    if (_counter >= (_numberQuestions - 1)) {
      resetQuizz();
    } else {
      setState(() {
        _counter++;
        _randomButton.shuffle();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.lightBlueAccent,
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            _buildScore(),
            QuizzBuilder.buildQuestion(
                quizzController.getQestionText(_counter)),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: _randomButton,
            )
          ],
        ),
      ),
    );
  }

  Widget _buildScore() {
    return Padding(
      padding: const EdgeInsets.only(
        left: 20.0,
        right: 20.0,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: _score,
      ),
    );
  }
}
